const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const Dishes = require('../models/dishes');
const authenticate = require('../authenticate');
const cors = require('./cors');

const dishRouter = express.Router();
dishRouter.use(bodyParser.json());
dishRouter.route('/')
.options(cors.corsWithOptions ,(req,res)=>{res.sendStatus(200);})
.get(cors.cors,(req,res,next)=>{
    // res.end('will send all dishes to you');
    Dishes.find({})
    .populate('comments.author')
     .then((dishes)=>{
         res.statusCode =200;
         res.setHeader('Content-Type','application/json');
         res.json(dishes);
     },err=>{next(err)})
     .catch(err=>{next(err)})
})
.post(cors.corsWithOptions,authenticate.verifyUser, authenticate.verifyAdmin,(req, res, next) =>{
    console.log(req.body);
            Dishes.create(req.body)
            .then(dish=>{
                console.log("Dish created",dish);
                res.statusCode=200;
                res.setHeader('Content-Type','application/json');
                res.json(dish)
            },err=>{next(err)})
            .catch(err=>{
                next(err);
                err.statusCode = 403;
            })
           .catch(err=>{err.statusCode=403;})
})
.put(cors.corsWithOptions,authenticate.verifyUser, (req, res, next) =>{
    res.statusCode = 403;
    res.end('PUT operation is not supported');
})
.delete(cors.corsWithOptions,authenticate.verifyUser,authenticate.verifyAdmin, (req, res, next) =>{
    Dishes.remove({})
    .then(resp=>{
        res.statusCode=200;
        res.setHeader('Content-Type','application/json');
        res.json(resp)
    },err=>{next(err)})
    .catch(err=>{next(err)})
});


//assignment task one

dishRouter.route('/:dishId')
.options(cors.corsWithOptions ,(req,res)=>{res.sendStatus(200);})
.get(cors.cors,(req,res,next)=>{
    Dishes.findById(req.params.dishId)
    .populate('comments.author')
    .then(dish=>{
        res.statusCode=200;
        res.setHeader('Content-Type','application/json');
        res.json(dish)
    },err=>{next(err)})
    .catch(err=>{next(err)})
})
.post(cors.corsWithOptions,authenticate.verifyUser, (req, res, next) =>{
    res.statusCode = 403;
    res.end('POST operation is not supported at dish: '+req.params.dishId);
})
.put(cors.corsWithOptions,authenticate.verifyUser, (req, res, next) =>{
    Dishes.findByIdAndUpdate(req.params.dishId,{$set:req.body},{new : true})
    .then(dish=>{
        res.statusCode=200;
        res.setHeader('Content-Type','application/json');
        res.json(dish)
    },err=>{next(err)})
    .catch(err=>{next(err)})
})
.delete(cors.corsWithOptions,authenticate.verifyUser, (req, res, next) =>{
    Dishes.findByIdAndRemove(req.params.dishId)
    .then(dish=>{
        res.statusCode=200;
        res.setHeader('Content-Type','application/json');
        res.json(dish)
    },err=>{next(err)})
    .catch(err=>{next(err)})
});

dishRouter.route('/:dishId/comments')
.options(cors.corsWithOptions ,(req,res)=>{res.sendStatus(200);})
.get(cors.cors,(req,res,next)=>{
    // res.end('will send all dishes to you');
    Dishes.findById(req.params.dishId)
    .populate('comments.author')
     .then((dish)=>{
         if(dish !=null){
            res.statusCode =200;
            res.setHeader('Content-Type','application/json');
            res.json(dish.comments);
         }
         else{
             err = new Error("Dish "+req.params.dishId+" not found")
             err.statusCode = 404;
             return next(err);
         }

     },err=>{next(err)})
     .catch(err=>{next(err)})
})
.post(cors.corsWithOptions,authenticate.verifyUser, (req, res, next) =>{
    console.log(req.body);
    
    Dishes.findById(req.params.dishId)
    .then(dish=>{
        if(dish !=null){
            // l authenricate.verfiyUser di bt7ot l user id fe l body mn nfsha fa ana b5do l2ni b7tago
            req.body.author = req.user._id;
            dish.comments.push(req.body);
            dish.save()
            .then(dish=>{
                Dishes.findById(dish._id)
                .populate('comments.author')
                .then(dish=>{
                    res.statusCode =200;
                    res.setHeader('Content-Type','application/json');
                    res.json(dish);
                })
            },err=>{console.log(err)})
            
         }
         else{
             err = new Error("Dish "+req.params.dishId+" not found")
             err.statusCode = 404;
             return next(err);
         }
    },err=>{next(err)})
    .catch(err=>{
        next(err)
        // res.statusCode = 403;
    })
})
.put(cors.corsWithOptions,authenticate.verifyUser, (req, res, next) =>{
    res.statusCode = 403;
    res.end('PUT operation is not supported/ '+req.params.dishId+'/comments');
})
.delete(cors.corsWithOptions,authenticate.verifyUser, (req, res, next) =>{
    Dishes.findById(req.params.dishId)
    .then(dish=>{
        if(dish !=null){
            for(var i =(dish.comments.length -1); i<=0 ; i--) {
                dish.comments.id(dish.comments[i]._id).remove();
                
            } 
            dish.save()
            .then(dish=>{
                res.statusCode =200;
                res.setHeader('Content-Type','application/json');
                res.json(dish);
            },err=>{console.log(err)})
         }
         else{
             err = new Error("Dish "+req.params.dishId+" not found")
             err.statusCode = 404;
             return next(err);
         }
    },err=>{next(err)})
    .catch(err=>{next(err)})
});



dishRouter.route('/:dishId/comments/:commentId')
.options(cors.corsWithOptions ,(req,res)=>{res.sendStatus(200);})
.get(cors.cors,(req,res,next)=>{
    Dishes.findById(req.params.dishId)
    .populate('comments.author')
    .then(dish=>{
        if(dish !=null && dish.comments.id(req.params.commentId) !=null){
            res.statusCode =200;
            res.setHeader('Content-Type','application/json');
            res.json(dish.comments.id(req.params.commentId));
         }
         else if (dish == null){
             err = new Error("Dish "+req.params.dishId+" not found")
             err.statusCode = 404;
             return next(err);
         }
         else{
            err = new Error("Comment "+req.params.commentId+" not found")
            err.statusCode = 404;
            return next(err);  
         }
    },err=>{next(err)})
    .catch(err=>{next(err)})
})
.post(cors.corsWithOptions,authenticate.verifyUser, (req, res, next) =>{
    res.statusCode = 403;
    res.end('POST operation is not supported at dish: '+req.params.dishId +'/comments/'+req.params.commentId);
})
.put(cors.corsWithOptions,authenticate.verifyUser, (req, res, next) =>{
    Dishes.findById(req.params.dishId)
    .then(dish=>{
        console.log("data >>>"+dish.comments.id(req.params.commentId).author._id);
        console.log("data >>>"+typeof(dish.comments.id(req.params.commentId).author._id));
        console.log("equal>>>>"+req.user._id);
        console.log("equal>>>>"+typeof(req.user._id));
        
        if(dish !=null && dish.comments.id(req.params.commentId) !=null){
            const authorId = JSON.stringify(dish.comments.id(req.params.commentId).author._id);
            const userId= JSON.stringify(req.user._id);
            console.log("author>>>"+authorId);
            console.log("req.user id >>>>"+req.user._id);
            console.log("if >>>"+authorId == req.user._id);
            
            if(authorId == userId){
                console.log("equal2>>>>"+req.user._id);
                
                if(req.body.rating){
                    dish.comments.id(req.params.commentId).rating = req.body.rating;
                }
                if(req.body.comment){
                    dish.comments.id(req.params.commentId).comment = req.body.comment;
                }
                dish.save()
                .then(dish=>{
                    res.statusCode =200;
                    res.setHeader('Content-Type','application/json');
                    res.json(dish);
                },err=>{console.log(err)})
            }
            else{
                res.statusCode = 404;
                 res.send("you are not allowed");
            }
         }
         else if (dish == null){
             err = new Error("Dish "+req.params.dishId+" not found")
             err.statusCode = 404;
             return next(err);
         }
         else{
            err = new Error("Comment "+req.params.commentId+" not found")
            err.statusCode = 404;
            return next(err);  
         }
    },err=>{next(err)})
    .catch(err=>{next(err)})
})
.delete(cors.corsWithOptions,authenticate.verifyUser, (req, res, next) =>{
    Dishes.findById(req.params.dishId)
    .then(dish=>{
        const authorId = JSON.stringify(dish.comments.id(req.params.commentId).author._id);
        const userId= JSON.stringify(req.user._id);
        console.log("author>>>"+authorId);
        console.log("req.user id >>>>"+req.user._id);
        console.log("if >>>"+authorId == req.user._id);
        if(dish !=null && dish.comments.id(req.params.commentId) !=null){
            if(authorId == userId){
                dish.comments.id(req.params.commentId).remove();

                dish.save()
                .then(dish=>{
                    Dishes.findById(dish_id)
                    .populate('comments.author')
                    .then(dish=>{
                        res.statusCode =200;
                        res.setHeader('Content-Type','application/json');
                        res.json(dish);
                    })
    
                },err=>{console.log(err)})
            }
            else{
                res.statusCode = 404;
                res.send("you are not allowed");  
            }
         }
         else if (dish == null){
            err = new Error("Dish "+req.params.dishId+" not found")
            err.statusCode = 404;
            return next(err);
        }
        else{
           err = new Error("Comment "+req.params.commentId+" not found")
           err.statusCode = 404;
           return next(err);  
        }
    },err=>{next(err)})
    .catch(err=>{next(err)})
});
module.exports = dishRouter;