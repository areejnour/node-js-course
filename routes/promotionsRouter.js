const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const promotion = require('../models/promotions');
var authenticate = require('../authenticate');
const cors = require('./cors');


const promotionRouter = express.Router();
promotionRouter.use(bodyParser.json());
promotionRouter.route('/')
.options(cors.corsWithOptions ,(req,res)=>{res.sendStatus(200);})
.get(cors.cors,(req,res,next)=>{
    promotion.find({})
    .then(promos=>{
        res.statusCode=200;
        res.setHeader('Content-Type','application/json');
        res.json(promos);
    },err=>{next(err)})
    .catch(err=>{next(err)})
})
.post(cors.corsWithOptions,authenticate.verifyUser,authenticate.verifyAdmin, (req, res, next) =>{
    console.log(req.body);
    promotion.create(req.body).then(promo=>{
        res.statusCode=200;
        res.setHeader('Content-Type','application/json');
        res.json(promo);
    },err=>{next(err)})
    .catch(err=>{next(err)})
})
.put(cors.corsWithOptions,authenticate.verifyUser,authenticate.verifyAdmin, (req, res, next) =>{
    res.end('PUT operation is not supported');
})
.delete(cors.corsWithOptions,authenticate.verifyUser,authenticate.verifyAdmin, (req, res, next) =>{
    promotion.remove({}).then(resp=>{
        res.statusCode=200;
        res.setHeader('Content-Type','application/json');
        res.json(resp);
    },err=>{next(err)})
    .catch(err=>{next(err)})
});


//assignment task one

promotionRouter.route('/:promoId')
.options(cors.corsWithOptions ,(req,res)=>{res.sendStatus(200);})
.get(cors.cors,(req,res,next)=>{
    promotion.findById(req.params.promoId).then(promo=>{
        res.statusCode=200;
        res.setHeader('Content-Type','application/json');
        res.json(promo);
    },err=>{next(err)})
    .catch(err=>{next(err)})
})
.post(cors.corsWithOptions,authenticate.verifyUser, (req, res, next) =>{
    res.end('POST operation is not supported at promotion: '+req.params.promoId);
})
.put(cors.corsWithOptions,authenticate.verifyUser,authenticate.verifyAdmin, (req, res, next) =>{
    promotion.findByIdAndUpdate(req.params.promoId,{$set:req.body},{new:true})
    .then(promo=>{
        res.statusCode=200;
        res.setHeader('Content-Type','application/json');
        res.json(promo);
    },err=>{next(err)})
    .catch(err=>{next(err)})
})
.delete(cors.corsWithOptions,authenticate.verifyUser,authenticate.verifyAdmin, (req, res, next) =>{

promotion.findByIdAndRemove(req.params.promoId)
.then(promo=>{
    res.statusCode=200;
    res.setHeader('Content-Type','application/json');
    res.json(promo);
},err=>{next(err)})
.catch(err=>{next(err)})
});
module.exports = promotionRouter;

