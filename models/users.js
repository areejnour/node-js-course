var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var passportLocalMongoose= require('passport-local-mongoose');

var user = new Schema({
    firstname:{
        type:String,
        defult:''
    },
    lastname:{
        type:String,
        defult:''
    },
    facebookId:String,
    admin :{
        type:Boolean,
        default:false
    }
});
//this will add the username and (hashed) password to the user schema
user.plugin(passportLocalMongoose);
module.exports = mongoose.model('User',user);




